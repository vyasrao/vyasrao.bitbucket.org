// function to get a section based on position
function getSectionByPosition(objects, position){
    
    if (position < 0)
        return;

    for (var i = 0; i < objects.length; i++) {
        // check if section is unique
        if (objects[i].position == position) {
            return objects[i];
        }
    }
    return;
}

// check duplicate name/ title
function isUnique(objects, value){
    // check section duplicate name
    for (var i = 0; i < objects.length; i++) {
        // check if unique
        if (objects[i].name.toUpperCase() == value.toUpperCase()) {
            return false;
        }
    }
    return true;
}