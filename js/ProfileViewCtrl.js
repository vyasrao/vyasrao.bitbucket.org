app.controller('ProfileViewCtrl', ['$scope', function ($scope) {

    /*
    $scope.sections: variable to hold the sections
    name: section title
    position: hols the current position of the section. dragging, editing and deleting section will change the position. helpful for database insertion and ordering on page render. positions starts from 0
    type: 'N' = New Section, 'D' = Default Section
    fields: contains fields of a section
    field.name = field title
    field.position: position for the field

    $scope.sections has to be filled initially by a web service call/ etc
    */
    $scope.sections = [
        {
            name: "Bio Data",
            type: "N",
            position: 0,
            fields:
                [
                    { name: "Father's Name", position: 2},
                    { name: "Full Name", position: 0},
                    { name: "Date Of Birth", position: 1},
                    { name: "Languages", position: 3 },
                    { name: "Contact Number", position: 4},
                    { name: "Email ID", position: 5},
                    { name: "Contact Address", position: 6},
                    { name: "Facebook ID", position: 7}
                ]
        },
        {
            name: "Eduction",
            type: "N",
            position: 1,
            fields: [
                { name: "Course", position: 0 },
                { name: "Branch", position: 1 },
                { name: "Specialization", position: 2 },
                { name: "University", position: 3 }
            ]
        },
        {
            name: "Employment",
            type: "N",
            position: 2,
            fields: [
                { name: "Company Name", position: 0 },
                { name: "Address", position: 1 }
            ]
        }
    ];

    // temporary variable to hold section details for add, edit & delete
    $scope.placeholder = {
        name: "",
        section: "",
        placement: "A",
        type: "N",
        edit: false
    };

    // variable to handle field additions
    $scope.newField = {
        name: "",
        section: null
    }

    // add new section to the sections container
    $scope.sections.add = function () {

        // check for duplicate section name
        if (!isUnique($scope.sections, $scope.placeholder.name)) {
            alert("Duplicate Section Exists.");
            return false;
        }

        var position = 0;

        // set position of the section and place the section as per selection
        // check if sections available, else position = 0
        if ($scope.placeholder.placementSection != null && $scope.placeholder.placementSection != "") {

            var tempPosition = parseInt($scope.placeholder.placementSection.position);

            // set position of the current section
            position = $scope.placeholder.placement == "A" ? tempPosition : tempPosition + 1;

            // loop through positions and increment proceeding positions
            $scope.sections.forEach(function (section) {

                if (section.position >= position) {
                    // increase the position of all proceeding sections
                    section.position = parseInt(section.position) + 1;
                }
            });
        }

        // add new section to existing section
        $scope.sections.push({
            name: $scope.placeholder.name,
            type: $scope.placeholder.type,
            position: position,
            fields: []
        });

        $('#sectionModal').modal('hide');

        $scope.placeholder.name = "";
        $scope.placeholder.type = "";
    }

    // open add modal
    $scope.sections.openAddModal = function () {

        // same modal is used to add and edit section, hence build variables based on type
        $scope.placeholder.edit = false;
        $scope.placeholder.name = "";
        $scope.placeholder.type = "N";
        $scope.placeholder.placement = "B";

        // by default add a section below the last section, user can change the same while adding new section
        $scope.placeholder.placementSection = getSectionByPosition($scope.sections, $scope.sections.length - 1)

        $('#sectionModal').modal();
    }

    // open modal to edit a section. Hold data in placeholder for edit
    $scope.sections.openEditModel = function (section, index) {

        // same modal is used to add and edit section, hence build variables based on type
        $scope.placeholder.edit = true;
        $scope.placeholder.section = section;
        $scope.placeholder.name = section.name;
        $scope.placeholder.type = section.type;

        // placement is null by default, because user may simply change title without changing the position
        $scope.placeholder.placement = "";
        $scope.placeholder.placementSection = "";

        $('#sectionModal').modal();
    }

    // edit a section
    $scope.sections.edit = function () {

        $scope.placeholder.section.name = $scope.placeholder.name;
        $scope.placeholder.section.type = $scope.placeholder.type;

        // place the section as per selection, igonre if placement is same
        if ($scope.placeholder.placementSection != null
            && $scope.placeholder.placementSection != ""
            && $scope.placeholder.section != $scope.placeholder.placementSection) {

            var currentPosition = $scope.placeholder.section.position;
            var newPosition = parseInt($scope.placeholder.placementSection.position);

            // check if both old and new positions are same, ignore if true
            if (parseInt(currentPosition) != newPosition) {
                // set position of the current section

                // flag current section
                $scope.placeholder.section.position = -100;

                // if placing below a section
                if ($scope.placeholder.placement == "B") {
                    // loop through positions and increment preceeding positions
                    $scope.sections.forEach(function (section) {

                        if (parseInt(section.position) > parseInt(currentPosition) &&
                            parseInt(section.position) <= newPosition) {
                            // increase the position of all proceeding sections
                            section.position = parseInt(section.position) - 1;
                        }
                    });
                }
                    // if placing above a section
                else {
                    // loop through positions and increment preceeding positions
                    $scope.sections.forEach(function (section) {

                        if (parseInt(section.position) >= parseInt(newPosition) &&
                            parseInt(section.position) < currentPosition) {
                            // increase the position of all proceeding sections
                            section.position = parseInt(section.position) + 1;
                        }
                    });
                }

                // update current position of the section
                $scope.placeholder.section.position = newPosition;
            }
        }

        $('#sectionModal').modal('hide');
    }

    // open modal to delete a section. Hold the section and index for final delete
    $scope.sections.openDeleteModal = function (section, index) {
        $scope.placeholder.section = section;
        $scope.placeholder.index = index;
        $('#deleteSection').modal();
    }

    // delete a section
    $scope.sections.delete = function () {

        var position = parseInt($scope.placeholder.section.position);
        $scope.sections.splice($scope.sections.indexOf($scope.placeholder.section), 1);

        // change the position of all the proceeding sections
        $scope.sections.forEach(function (section) {
            if (parseInt(section.position) > position) {
                section.position = parseInt(section.position) - 1;
            }
        });

        $('#deleteSection').modal('hide');

        $scope.placeholder.section = "";
        $scope.placeholder.index = "";
    }

    // open modal to add new field. Hold the section in an variable to later push
    $scope.newField.openAddModal = function (section) {
        $scope.newField.section = section;
        $('#addNewField').modal();
    }

    // add new field to a section
    $scope.newField.add = function () {

        // check for duplicate section name
        if (!isUnique($scope.newField.section.fields, $scope.newField.name)) {
            alert("Duplicate Field Exists.");
            return false;
        }

        // add field
        $scope.newField.section.fields.push({
            name: $scope.newField.name,
            property: $scope.newField.property,
            // new field always at the end
            position: $scope.newField.section.fields.length
        });

        $('#addNewField').modal('hide');

        $scope.newField.name = "";
        $scope.newField.property = "";
    }

    // delete a field
    $scope.sections.deleteField = function (fields, field) {

        var position = parseInt(field.position);

        fields.splice(fields.indexOf(field), 1);

        // change the position of all the proceeding fields
        fields.forEach(function (field) {
            if (parseInt(field.position) > position) {
                field.position = parseInt(field.position) - 1;
            }
        });
    }

    // sort sections based on position. sometimes the position behaves like string instead of int, hence the function
    $scope.sorterSection = function (section) {
        return parseInt(section.position);
    };

    // sort fields based on position. sometimes the position behaves like string instead of int, hence the function
    $scope.sorterField = function (field) {
        return parseInt(field.position);
    };
}]);