app.controller('MenuCtrl', ['$scope', function($scope){

    $scope.menu = {};
    $scope.menu.title = "Regency University CRM";
    $scope.menu.items = [{
        title: "VIEWS", 
        url: "#", 
        subMenu: [
            {title: "PROFILE", url: "#"},
            {title: "SIGNUP FORM", url: "#"}
        ],
        current: true
    },
    {
        title: "PROPERTIES",
        url: "#",
        current: false
    },
    {
        title: "DEFAULT PROPERTIES",
        url: "#",
        current: false
    }];
    
}]);