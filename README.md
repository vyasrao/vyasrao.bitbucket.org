# README #

Source repository for CRM Admin Profile View sceen. [click here](http://vyasrao.bitbucket.org/) for a demo.

### Assumptions ###

* Add Section: By default new section will be added below the last section, user can change the same.
* Edit Section: Made 'Order Position' blank, user can choose position  based on the need
* Add Fields: New field is always added at the last position
* Menu: Dummy menu, created just to showcase multiple controllers
* Save/ Cancel buttons: dummy buttons
* Section and field positions: Maintained a 'key' to store the current position of sections and fields for database push.